# frozen_string_literal: true

require_relative "lib/file_sv/version"

Gem::Specification.new do |spec|
  spec.name          = "file_sv"
  spec.version       = FileSv::VERSION
  spec.authors       = ["Samuel Garratt"]
  spec.email         = ["samuel.garratt@integrationqa.com"]

  spec.summary       = "REST service virtualisation through file structure."
  spec.description   = "Create a virtual REST service through file structure. Customize it more
through ERB and special file names"
  spec.homepage      = "https://gitlab.com/samuel-garratt/file_sv"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.4.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/samuel-garratt/file_sv"
  spec.metadata["changelog_uri"] = "https://gitlab.com/samuel-garratt/file_sv/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir.glob("{lib,exe}/**/*")
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.add_dependency "faker"
  spec.add_dependency "ougai"
  spec.add_dependency "rackup"
  spec.add_dependency "sinatra"
  spec.add_dependency "sinatra-docdsl"
  spec.add_dependency "thor"
  spec.add_dependency "puma"
end
