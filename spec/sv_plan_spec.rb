# frozen_string_literal: true

RSpec.describe SvPlan do
  before { described_class.create("spec/example_service") }

  it "calculates endpoints correctly" do
    expect(described_class.endpoints.keys).to have_attributes(count: 9)
  end

  it "excludes README" do
    expect(described_class.file_list).not_to include('/README.md')
  end

  it "calculates methods correctly" do
    expect(described_class.endpoints["/specify_method"].keys).to eq %w[get post]
  end
end
