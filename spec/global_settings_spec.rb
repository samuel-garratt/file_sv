# frozen_string_literal: true

RSpec.describe GlobalSettings do
  describe "#ignored_status?" do
    after { described_class.ignore_status_codes = nil }

    it "handles null" do
      described_class.ignore_status_codes = nil
      expect(described_class.ignored_status?(500)).to be nil
    end

    it "ignores 500 if it's covered in list" do
      described_class.ignore_status_codes = "4*,5*, 600"
      expect(described_class.ignored_status?(500)).to be true
    end

    it "allows 200 if it's not covered in list" do
      described_class.ignore_status_codes = "4*,5*, 600"
      expect(described_class.ignored_status?(200)).to be false
    end
  end
end
