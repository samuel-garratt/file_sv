# frozen_string_literal: true

RSpec.describe PlannedEndpoint, ".path" do
  it "has root folder is /" do
    endpoint = described_class.new "/README.md"
    expect(endpoint).to have_attributes(path: "/")
  end

  it "correctly determines sub folder" do
    endpoint = described_class.new "/sub_path/single_response.json"
    expect(endpoint).to have_attributes(path: "/sub_path")
  end

  it "replaces % with : for dynamic paths" do
    endpoint = described_class.new "/person/%id/detail/response.json"
    expect(endpoint).to have_attributes(path: "/person/:id/detail")
  end

  it "handles ." do
    endpoint = described_class.new "/.well-known/openid-configuration/test.txt"
    expect(endpoint).to have_attributes(path: "/.well-known/openid-configuration")
  end
end
