# frozen_string_literal: true

RSpec.describe PlannedEndpoint, ".status_code" do
  it "get has default of 200" do
    endpoint = described_class.new "/README.md"
    expect(endpoint).to have_attributes(status_code: 200)
  end

  it "defaults to 204 (GlobalSettings.empty_body_status) for empty body" do
    endpoint = described_class.new "/status_code/post_empty_body.json"
    expect(endpoint).to have_attributes(status_code: 204)
  end

  it "defaults to GlobalSettings.empty_body_status" do
    endpoint = described_class.new "/status_code/post_empty_body.json"
    expect(endpoint).to have_attributes(status_code: GlobalSettings.empty_body_status)
  end

  it "post has default of 201" do
    endpoint = described_class.new "/specify_method/post.json"
    expect(endpoint).to have_attributes(status_code: 201)
  end

  it "can set specify status code in filename" do
    endpoint = described_class.new "/status_code/209_status.xml"
    expect(endpoint).to have_attributes(status_code: 209)
  end

  it "throws error when 2 status codes specified" do
    expect do
      described_class.new "/specify_method/201_and_204.json"
    end.to raise_error FileSv::FileNameError, /status code/
  end
end
