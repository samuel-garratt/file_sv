# frozen_string_literal: true

RSpec.describe PlannedEndpoint, ".method" do
  it "is the default method" do
    endpoint = described_class.new "/alternate_response/response.json"
    expect(endpoint).to have_attributes(method: "get")
  end

  it "set to post if within filename" do
    endpoint = described_class.new "/specify_method/post.json"
    expect(endpoint).to have_attributes(method: "post")
  end

  it "throws error when 2 methods specified" do
    expect do
      described_class.new "/specify_method/get_and_post.json"
    end.to raise_error FileSv::FileNameError, /method/
  end
end
