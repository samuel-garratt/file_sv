# frozen_string_literal: true

RSpec.describe PlannedEndpoint, ".content" do
  it "Interprets through ERB" do
    endpoint = described_class.new "/alternate_response/response.json"
    expect(endpoint.content(binding)).not_to include "<%="
  end
end
