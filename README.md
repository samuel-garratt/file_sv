# File Service Virtualization

The idea behind this library is to make creating service virtualization intuitive and easy for people without
coding experience. Using a simple file structure one kind define how a virtual service works to enable testing
different scenarios easy. 

It is open source and therefore free and can be easily run with docker. Making changes should be easy and if dynamic values are required they can be also done 

## Get started 

Within a folder with some files and folders that describe some mock endpoints simply run

`docker run -v $(pwd):/home/app/serve -p 4567:4567 --rm -it registry.gitlab.com/samuel-garratt/file_sv:slim`

to run a simple virtual service at port 4567.  

You can start by running this from the example folder `spec/example_service` of this repo to get a feel for it by following https://gitlab.com/samuel-garratt/file_sv/-/wikis/Practice-using-File-sv


## Installation

Install it as a ruby gem with:

    $ gem install file_sv

or run pull it in docker with the following syntax

`docker pull registry.gitlab.com/samuel-garratt/file_sv`

## Usage

### Via ruby gem
By installing this gem, an executable `file_sv` is installed.

This `exe` is designed to run from a file structure that defines a virtual service. 

Following is a simple file structure that shows how this works:

```
/response1.json => GET response 1 at path '/' (split 50% between 1 and 2) Return 200 by default
/response2.json => GET response 2 (the purpose of this splitting is to make it easy to simulate how a UI or other system responds to diff behaviours)
/post.json => Response for POST request returns 201 by default
/patch.json => Response for PATCH request, empty file so returns 204
/options_500.json => OPTIONS response, returns 500 status code
/people/response.json => GET response at path '/people'
/org/%id/detail.json => GET response to 'org/ANY_PATH' making a variable @id available at that path
```

To see the plan of this folder from that folder run `file_sv plan .` 

> The `.` is the path to where the virtual service is defined

To serve that folder run `file_sv serve .`. 

> The `sinatra` gem is used to host this. By default the port used is `4999`. 
> Use the `PORT` environment variable to change the port. E.g., run on port 8000 `PORT=8000 file_sv serve .`   

Run `file_sv help` to see other commands

### Reserve automatically after making changes to files

To have application run automatically based on changes use the `rerun` gem.

E.g

`rerun --pattern **.* file_sv serve .` 

> This done automatically in the larger docker image

### Via Docker

`docker run -v $(pwd):/home/app/serve -p 4567:4567 --rm -it registry.gitlab.com/samuel-garratt/file_sv`

> This would use the current folder as the source to config the virtual service and run
> it on port 4567 of the local machine 

To see the plan in docker use the following syntax

`docker run -v $(pwd):/home/app/serve --rm -it registry.gitlab.com/samuel-garratt/file_sv plan .`

> Note the default image is larger and performs auto-retry. Use the `slim` tag if this is not needed

#### Dockerising your mock service

To create your own dockerfile where you can have your own custom mock running you can create a new docker image, COPY the folder into it and set
the command to run the service. 
The following would work if you have your mock folders within a folder called `mock_backend`.
```Dockerfile
FROM registry.gitlab.com/samuel-garratt/file_sv:slim
WORKDIR /home/app/service
COPY mock_backend mock_backend
COPY file_sv.yaml file_sv.yaml

EXPOSE 4567
ENTRYPOINT ["ruby", "/home/app/service/exe/file_sv"]
CMD ["serve"]
```

To serve based on the `mock_backend` you need a `file_sv.yaml` with the contents

```yaml
global:
  serving_folder: mock_backend
```

### Defaults

See `file_sv.yaml` for the default values. These can be overridden by creating your own `file_sv.yaml` at
the root of your virtual service folder. 

### Logging
If you are running this with multiple instances in an environment such as Kubernetes set the logging to structured by setting `LOG` environment variable to `structured`.

Use `LOG_LEVEL` environment variable to change the log level, default being `INFO`. 

### Serve locally over SSL

There are times when you need to serve the virtual server over https without the use of reverse proxy.

* Create a certificate for local host

`openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout pkey.pem -out cert.crt -subj '/CN=localhost'`

## Keywords
A file is broken up by parts separated by `_`. The following are used within a filename to configure how it responds. REST method names like `get put post patch delete options` will change the HTTP method the mock responds to. Numbers between 100 and 999 will change the status code returned. The word `delay` + a number will make the response only return after a particular time.

## TODO

* Handle extension methods
* Handle media types
* Tutorial
* Redirect/proxy
* Basic auth? (in file_sv.yaml)

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on Gitlab at https://gitlab.com/samuel-garratt/file_sv. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/samuel-garratt/file_sv/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the FileSv project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/samuel-garratt/file_sv/blob/master/CODE_OF_CONDUCT.md).
