
require "sinatra"
require "puma"
require "json"
require "securerandom"

set :server, :puma

def extract_models
  error_msg = "Make 'models' environment variable an array (e.g ['model1', 'model2'])"
  raise "Please set 'models' ENV variable. #{error_msg}" if ENV['models'].nil?

  models = eval(ENV['models'])
  raise error_msg unless models.is_a? Array 

  models
end

class Entities
  @models = {}
  class << self
    attr_accessor :models
  end
end

extract_models.each do |model|
  Entities.models[model.to_sym] = {}
end

puts "Modelling #{Entities.models.keys}"

Entities.models.keys.each do |model|
  def has_id?(model, id)
    Entities.models[model].has_key?(id)
  end

  def not_found(id)
    [404, JSON.generate({error: "'#{id}' not found"})]
  end

  post "/#{model}" do
    id = SecureRandom.uuid
    Entities.models[model][id] = JSON.parse(request.body.read)
    [201, id]
  end

  get "/#{model}" do
    puts params
    puts params.class
    puts params == {}
    return [200, Entities.models[model].to_s] if params == {}
    Entities.models[model].values.find_all { |val| val[params.keys[0]] == params.values[0] }
    rescue Exception
      [404, "Nothing found using #{params}. Only first param considered"]
  end

  get "/#{model}/:id" do |id|
    return not_found(id) unless has_id?(model, id)
    JSON.generate(Entities.models[model][id])
  end

  put "/#{model}/:id" do |id|
    return not_found(id) unless has_id?(model, id)
    Entities.models[model][id] = JSON.parse(request.body.read)
    204
  end

  delete "/#{model}/:id" do |id|
    return not_found(id) unless has_id?(model, id)
    Entities.models[model].delete(id)
    204
  end
end