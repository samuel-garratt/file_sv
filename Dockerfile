FROM ruby:3.3-alpine

RUN apk add --update build-base libffi-dev
RUN addgroup -S appgroup && adduser -S app -G appgroup

RUN chmod -R 777 /usr/local/bundle

USER app

EXPOSE 4567

RUN mkdir /home/app/service
WORKDIR /home/app/service
COPY . /home/app/service
RUN gem install bundler
RUN bundle config set without 'dev'
RUN bundle install
# For rerunning when file changes
RUN gem install rerun
RUN gem install wdm
RUN mkdir /home/app/serve
WORKDIR /home/app/serve

ENTRYPOINT ["rerun", "--dir", "/home/app/serve", "--pattern", "**/*.*", "ruby", "/home/app/service/exe/file_sv"]
CMD ["serve"]
