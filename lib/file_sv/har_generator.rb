require_relative '../file_sv'
require 'json'
require 'fileutils'

class HarGenerator
  
  attr_accessor :har_file_location

  attr_accessor :mock_service_base

  attr_accessor :output_folder
  # @return [String] Har file content
  attr_accessor :content

  attr_accessor :entries

  def initialize(har_file_location, mock_service_base, output_folder)
	self.har_file_location = har_file_location
	self.mock_service_base = mock_service_base
	self.output_folder = output_folder
	self.content = File.read(har_file_location)
	parse_har
  end

  def parse_har
	parsed = JSON.parse(self.content)
	pages = parsed['log']['pages']
	self.entries = parsed['log']['entries']
	FileSv.logger.info "Found #{self.entries.count} entries in #{self.har_file_location}"
  end

  def generate
    self.entries.each do |entry|
	request = entry['request'] 	
	response = entry['response']
	
	request_url = request['url']
	if (!request_url.include?(self.mock_service_base)) 
		next;
	end	
	
	path = request_url[self.mock_service_base.length..-1]
	path_folder = path.split('.')[0]
	
	method = request['method']
	status_code = response['status']
	content = response['content']
	mime_type = content['mimeType']
	extension = mime_type.split('/').last
	
	extension = 'js' if (extension == 'javascript')
	
	location = "#{self.output_folder}/#{path_folder}/#{method}_#{status_code}.#{extension}"
	create_file_at "./#{location}", extension, content['text']
	end
  end	

  # Create folder if there's not a file already there.
  # Will create parent folder if necessary.
  # @param [String] folder Folder to create
  def create_folder(folder)
	if File.exist? folder
		warn "!! #{folder} already exists and is not a directory" unless File.directory? folder
	else
		FileUtils.mkdir_p folder
		puts "Created folder: #{folder}"
	end
  end

  def create_file_at(file_path, extension, content)
	file_path = file_path.gsub('//', '/')
	folder_name = File.split(file_path).first
	folder_name += ".#{extension}" if (extension == '.css' || extension == '.js')
	create_folder folder_name
	puts "Creating response at #{file_path}"
	File.open("#{folder_name}/#{File.split(file_path).last}", 'w') { |f| f.puts content }
  end  
end