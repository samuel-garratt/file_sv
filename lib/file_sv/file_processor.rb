# frozen_string_literal: true

require_relative "sv_plan"
# General render for any non special files
class FileProcessor
  class << self
    # Process file, adding it to plan
    def process(filename)
      endpoint = PlannedEndpoint.new(filename)
      SvPlan + endpoint unless GlobalSettings.ignored_status? endpoint.status_code
    end
  end
end
