# frozen_string_literal: true

# Responsible for loading all files in service directory and creating service
module ServiceLoader
  # @return [String] Folder where service served from
  @serving_folder = "."

  class << self
    # Create virtual service plan based on folder
    def create_plan_for(folder)
      SvPlan.create folder
      puts SvPlan.show
    end

    # Inspect plan
    def inspect(folder)
      create_plan_for folder
      puts SvPlan.inspect
    end

    # Serve plan
    def serve_plan(thor_options)
      require "sinatra"
      require_relative "virtual_server"
      GlobalSettings.key = thor_options[:key] if thor_options[:key]
      GlobalSettings.cert = thor_options[:crt] if thor_options[:crt]
      VirtualServer.run!
    end
  end
end
