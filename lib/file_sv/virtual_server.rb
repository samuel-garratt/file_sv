# frozen_string_literal: true

require "puma"
require "sinatra"
require "docdsl"
require "openssl"

# Virtual server hosting virtual service defined through files
class VirtualServer < Sinatra::Base
  set :server, :puma
  enable :logging if ENV["debug"] == "true"
  set :bind, "0.0.0.0"
  helpers ServerUtils

  register Sinatra::DocDsl

  page do
    title "File SV"
    header "Service virtualization created from #{Dir.pwd}"
    introduction 'Created using file_sv. See <a href="https://gitlab.com/samuel-garratt/file_sv">File SV</a>
for more details'
  end

  before do
    @request_id = SecureRandom.uuid
    headers["X-Correlation-Id"] = @request_id
    headers["Access-Control-Allow-Origin"] = "*"
    log({ msg: "Request" }) if request.fullpath != "/"
  end

  get "/favicon.ico" do
    send_file File.join(__dir__, "file_sv.ico")
  end

  doc_endpoint "/docs"

  # Output for endpoint, either a file or text content
  # @param [PlannedEndpoint] endpoint Planned endpoint to serve
  def output_for(endpoint, binding)
    endpoint.file? ? send_file(endpoint.serving_file_name) : endpoint.content(binding)
  end

  def append_content_type(endpoint)
    if endpoint.file_path.end_with? ".json"
      content_type :json
    elsif endpoint.file_path.end_with? ".xml"
      content_type :xml
    elsif endpoint.file_path.end_with? ".css"
      content_type :css
    elsif endpoint.file_path.end_with? ".js"
      content_type :js
    end
  end

  # Log endpoint. Return content and status code defined by endpoint
  # @param [PlannedEndpoint] endpoint Planned endpoint to serve
  def serve(endpoint, id = nil)
    @id = id
    append_content_type(endpoint)
    if ENV["debug"] == "true"
      message = "Using endpoint based on file #{endpoint.serving_file_name}."
      message += " Using param '#{@id}'" if id
      FileSv.logger.debug message
    end
    sleep endpoint.delay if endpoint.delay > 0
    [endpoint.status_code, output_for(endpoint, binding)]
  end

  SvPlan.endpoints.each do |endpoint_path, endpoint_value|
    endpoint_value.each do |_method_name, endpoints|
      endpoint_base = endpoints[0]
      documentation "Endpoint #{endpoint_base.path}" do
        response "#{endpoints.size} kinds of response"
      end
      if endpoint_base.path.include? "#{File::Separator}:"
        send(endpoint_base.method, endpoint_base.path) do |id|
          response["Access-Control-Allow-Origin"] = "*"
          endpoint = endpoints.sample
          @params = params
          log({ msg: "Response", status: endpoint.status_code, endpoint: endpoint.serving_file_name }) if request.fullpath != "/"
          serve endpoint, id
        end
      else
        send(endpoint_base.method, endpoint_base.path) do
          response["Access-Control-Allow-Origin"] = "*"
          endpoint = endpoints.sample
          @params = params
          unless ENV["ignore_path"] && ("/#{ENV["ignore_path"]}" == endpoint_base.path)
            log({ msg: "Response", status: endpoint.status_code, endpoint: endpoint.serving_file_name }) if request.fullpath != "/"
          end
          serve endpoint
        end
      end
    end
    # options endpoint for CORS
    options endpoint_path do
      response["Allow"] = "*"
      response["Access-Control-Allow-Origin"] = "*"
      response["Access-Control-Allow-Methods"] = "*"
      response["Access-Control-Allow-Headers"] = "*"
    end
  end
end
