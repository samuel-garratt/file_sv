# frozen_string_literal: true

# typed: false

# Helper methods added to RestServer
module ServerUtils
  def log(messages)
    if FileSv.log_type == :ougai
      log_structured(messages)
    else
      message = messages.values.join(", ")
      FileSv.logger.info "#{message}, #{request.request_method} #{request.fullpath}, CorrelationId: #{@request_id}"
    end
  end

  private def log_structured(messages)
    log_msg = {
      method: request.request_method,
      path: request.fullpath,
      correlationId: @request_id
    }
    messages.each do |key, value|
      log_msg[key] = value
    end
    FileSv.logger.info(log_msg)
  end
end
