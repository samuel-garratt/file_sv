# frozen_string_literal: true

require "erb"
require "securerandom"
require "faker"
require "pathname"

# Endpoint planned to be served
class PlannedEndpoint
  # Location where endpoint will be hosted
  attr_accessor :path
  # @return [String] Path where content is sourced from
  attr_accessor :file_path
  # @return [String] REST Method
  attr_accessor :method
  # @return [Integer] HTTP Status code
  attr_accessor :status_code
  # @return [Integer] Delay before responding
  attr_accessor :delay

  # @return [Array]
  HTTP_METHODS = %w[get put post patch delete options].freeze

  # Represent a new endpoint
  def initialize(path)
    self.file_path = path
    self.path = serving_loc_for path
    @file = true if not_text?
    assign_params_from_name
    self.method ||= GlobalSettings.default_method
    self.delay ||= 0
    return if status_code

    set_default_status_code
  end

  def serving_loc_for(path)
    loc = File.split(path).first # TODO: Handle if path has a % at beginning of a folder
    loc.gsub("#{File::SEPARATOR}%", "#{File::SEPARATOR}:")
  end

  # @return [Boolean] Whether endpoint serves a file not a string
  def file?
    @file
  end

  # Return default status code for an empty body
  def default_empty_code
    return false if not_text?

    if content(binding).strip.empty?
      self.status_code = GlobalSettings.empty_body_status
      return true
    end

    return false
  rescue Encoding::CompatibilityError
    false
  end

  # Set default status code based on empty body or type of METHOD
  def set_default_status_code
    return if default_empty_code

    setting_class = FileSv.rest_methods[method.to_sym]
    raise NotImplementedError, "Unable to interpret method #{method}" unless setting_class

    self.status_code = setting_class.default_status
  end

  # @return [String] Filename without extension or containing folder
  def filename
    File.basename(file_path, ".*")
  end

  # Set attributes based on filename
  def assign_params_from_name
    file_parts = filename.split("_")
    assign_method_from file_parts
    assign_delay_from file_parts
    assign_status_from file_parts
  end

  # Set status code based on filename
  def assign_status_from(file_parts)
    status_parts = file_parts.find_all { |part| part.to_i > 99 && part.to_i < 1000 }
    if status_parts.size > 1
      raise FileSv::FileNameError, "Filename #{filename} has more than 1 status code #{status_parts}"
    end

    self.status_code = status_parts[0].to_i if status_parts.size == 1
  end

  # Set status code based on filename
  def assign_delay_from(file_parts)
    delay_parts = file_parts.find_all { |part| part.start_with?('delay') }
    if delay_parts.size > 1
      raise FileSv::FileNameError, "Filename #{filename} has more than 1 delay #{delay_parts}"
    end

    self.delay = delay_parts[0][5..-1].to_i if delay_parts.size == 1
  end

  # Set REST method used based on filename
  def assign_method_from(file_parts)
    method_parts = file_parts.find_all { |part| HTTP_METHODS.include? part }
    if method_parts.size > 1
      raise FileSv::FileNameError, "Filename #{filename} has more than 1 REST methods #{method_parts}"
    end

    self.method = method_parts[0] if method_parts.size == 1
  end

  def serving_file_name
    File.join(SvPlan.serving_folder, file_path)
  end

  # @return [Object] Content of file
  def content(binding)
    render_text(binding)
  end

  def not_text?
    extension = File.extname(file_path).delete(".")
    %w[ico png jpg mp4 mp3].include? extension
  end

  # @return [String] Render text
  def render_text(binding)
    ERB.new(File.read(serving_file_name)).result(binding)
  end

  def short_description
    desc = self.status_code.to_s
    desc += " (delay #{self.delay} sec)" if self.delay > 0
    return desc
  end
end
