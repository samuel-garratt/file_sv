# frozen_string_literal: true

# Settings to configure this gem
class GlobalSettings
  @default_method = "get"

  @empty_body_status = 204

  @ignore_files = "{*.md,Dockerfile}"

  @https = false

  @serving_folder = '.'

  @ignore_status_codes = nil
  class << self
    # @return [String] Default REST method when none specified by filename
    attr_accessor :default_method
    # @return [Integer] Default status of response when file is empty
    attr_accessor :empty_body_status
    # @return [Array] Expression representing files to ignore
    attr_accessor :ignore_files
    # @return [Boolean] Whether to serve https using self signed certificate. Deprecated now
    attr_accessor :https
    # @return [String] Path to HTTPS cert
    attr_accessor :cert
    # @return [String] Path to HTTPS key
    attr_accessor :key
    # @return [Array] List of http status codes to ignore
    attr_accessor :ignore_status_codes
    # @return [String] Path from current location to serve folders from
    attr_accessor :serving_folder

    # @param [Integer] status_code HTTP status code
    # @return [Boolean] Whether status code is currently ignored
    def ignored_status?(status_code)
      return unless ignore_status_codes

      ignore_status_codes.split(",").each do |code|
        regex = Regexp.new code.to_s
        result = status_code.to_s[regex]
        next unless result

        return true unless result.empty?
      end
      false
    end
  end
end

# Http settings that all HTTP method types inherit
module CommonHttpSettings
  attr_accessor :default_status
end

# Settings specific to GET
class GetSettings
  @default_status = 200
  extend CommonHttpSettings
end

# Settings specific to POST
class PostSettings
  @default_status = 201
  extend CommonHttpSettings
end

# Settings specific to PATCH
class PatchSettings
  @default_status = 200
  extend CommonHttpSettings
end

# Settings specific to PATCH
class PutSettings
  @default_status = 200
  extend CommonHttpSettings
end

# Settings specific to OPTIONS
class OptionsSettings
  @default_status = 200
  extend CommonHttpSettings
end

# Settings specific to DELETE
class DeleteSettings
  @default_status = 200
  extend CommonHttpSettings
end
