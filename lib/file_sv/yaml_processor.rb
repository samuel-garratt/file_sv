# frozen_string_literal: true

# Process YAML files
class YamlProcessor
  class << self
    # Process YAML file
    def process(filename)
      if filename == "/file_sv.yaml"
        puts "Overriding default config based on #{File.join(Dir.pwd, filename[1..])}"
        load_default_config filename[1..]
      else
        puts "Skipping #{filename}"
      end
    end
  end
end
