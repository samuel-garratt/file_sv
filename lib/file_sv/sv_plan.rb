# frozen_string_literal: true

require_relative "yaml_processor"
require_relative "file_processor"
# Holds plan of what virtual service will look like
class SvPlan
  @endpoints = {}
  @file_list = []
  class << self
    # @return [Hash] Endpoints included in plan. Key - endpoint, value - methods served under it
    attr_reader :endpoints
    # @return [String] Folder plan is served from
    attr_accessor :serving_folder

    # @return [String] List of files plan is built from
    attr_accessor :file_list

    # Create a plan new plan for a virtual service
    def create(folder)
      self.serving_folder = folder
      puts "Creating service based on files in #{folder}"
      file_list = Dir.glob("#{folder}/**/*.*", File::FNM_DOTMATCH) - Dir.glob("#{folder}/#{GlobalSettings.ignore_files}", File::FNM_DOTMATCH)        
      file_list.each do |file| 
        next if File.directory? file
        SvPlan.file_list << file
        process_file file
      end
    end

    # Process file, for the most part creating endpoint.method from it
    # @param [String] filename Path to file to process
    def process_file(filename)
      filename.slice! serving_folder
      extension = File.extname(filename)
      case extension
      when ".yaml" then YamlProcessor.process(filename)
      else
        FileProcessor.process(filename)
      end
    end

    # Show plan
    def show
      endpoint_desc = ""
      endpoints.sort { |a, b| a[0].casecmp b[0] }.each do |endpoint, methods|
        endpoint_desc += "#{endpoint} \n"
        methods.each do |method_name, endpoints|
          endpoint_desc += description_message method_name, endpoints
        end
      end

      "** VIRTUAL SERVICE PLAN **
Serving based on folder: #{Dir.pwd}. Related to folder executed: #{serving_folder}
#{endpoint_desc}"
    end

    # Inspect details
    def inspect
      "Endpoints: #{endpoints.inspect}"
    end

    # Add endpoint to plan
    # @param [PlannedEndpoint] other Endpoint to add to plan
    def +(other)
      @endpoints[other.path] ||= {}
      @endpoints[other.path][other.method] ||= []
      @endpoints[other.path][other.method] << other
    end

    private

    # @return [String] Message to description all endpoints for a method undera endpoint
    def description_message(method_name, endpoints)
      "  #{method_name.upcase} (#{endpoints.size} responses) #{endpoints.collect(&:short_description)}\n"
    end
  end
end
