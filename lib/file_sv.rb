# frozen_string_literal: true

require "yaml"
require "logger"
require "ougai"
require_relative "file_sv/version"
require_relative "file_sv/global_settings"
require_relative "file_sv/sv_plan"
require_relative "file_sv/service_loader"
require_relative "file_sv/planned_endpoint"
require_relative "file_sv/server_utils"

ENV["APP_ENV"] ||= "production"

# Create Service Virtualization from a simple file system
module FileSv
  # General error for FileSv
  class Error < StandardError; end

  # Error related to incorrect format of filename
  class FileNameError < Error; end

  if ENV["Log"] == "structured"
    @logger = Ougai::Logger.new($stdout)
    @log_type = :ougai
  else
    @logger = Logger.new($stdout)
    @log_type = :basic
  end
  LOGLEVELS = %w[DEBUG INFO WARN ERROR FATAL UNKNOWN].freeze
  log_level ||= LOGLEVELS.index ENV.fetch("LOG_LEVEL","INFO")
  @logger.level = log_level

  class << self
    # @return [Hash] Mapping of REST method names to setting classes
    def rest_methods
      {
        get: GetSettings, post: PostSettings, patch: PatchSettings, options: OptionsSettings,
        delete: DeleteSettings, put: PutSettings
      }
    end

    # @return [Logger] Logger
    attr_accessor :logger

    # @return Logger type
    attr_accessor :log_type
  end
end

CONFIG_FILE = "file_sv.yaml"

# Set values in global settings based on config
def load_default_config(file_path)
  unless File.exist? file_path
    FileSv.logger.debug "No config found at #{file_path}" if ENV["debug"] == "true"
    return
  end
  FileSv.logger.debug "Loading config from #{file_path}" if ENV["debug"] == "true"

  config = YAML.load_file file_path
  return unless config # Handle empty YAML file

  FileSv.logger.debug "Config #{config}" if ENV["debug"] == "true"
  config["global"]&.each do |key, value|
    FileSv.logger.debug "Setting #{key} to #{value}" if ENV["debug"] == "true"
    GlobalSettings.send("#{key}=", value)
  end

  load_rest_method_config config
end

# Load details of each REST method
def load_rest_method_config(config)
  FileSv.rest_methods.each do |method, setting_class|
    config[method.to_s]&.each { |key, value| setting_class.send("#{key}=", value) }
  end
end

# Set global params based on ENV vars
def set_based_on_env_vars
  GlobalSettings.instance_variables.each do |setting|
    setting_name = setting.to_s[1..]
    if ENV[setting_name]
      FileSv.logger.debug "Setting #{setting_name} to #{ENV[setting_name]}"
      GlobalSettings.send("#{setting_name}=", ENV[setting_name])
    end
  end
end

load_default_config CONFIG_FILE
set_based_on_env_vars
