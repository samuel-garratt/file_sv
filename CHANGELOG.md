## [Unreleased]
## [0.3.1]
- Fixed status code logged incorrectly

## [0.3.0]
- Better logging

## [0.2.1]
- Make production the default environment
- Fix and simplify param logging where 'id' is used in url

## [0.2.0]
- Each endpoint return allow origin header for when used running locally and have requests from different origins
- OPTIONS endpoint for each endpoint allowing all methods and origins

## [0.1.13]
- Generator to create folder/file structure mock content based on recording in a HAR file

## [0.1.12]
- Have default serving folder as '.' and configurable in file_sv.yaml as 'serving_folder'

## [0.1.11]
- Add 'css' or 'js' content type based on file extension name 
- Make response have custom delay for X seconds if 'delayX' is part of filename

## [0.1.10]
- Add 'json' or 'xml' content type based on file extension name 

## [0.1.9]
- Fix 'ignore_folders' that was not working

## [0.1.8]

- Handle folders with '.' in them and do so by default

## [0.1.7]

- Ability to use @params to get query parameters to base response on it
- Use puma instead of webrick
- Allow ignore logging of endpoint to be done via ENV vars "ignore_path" (to exclude a liveness path) and "debug"

## [0.1.6]

- Ability to exclude status codes based on 'ignore_status_codes' param
- Ability to set global params based on environment variables

## [0.1.5]

- Fix exclusion of markdown files
- Add ability to serve with self-signed certs
- Fixed missing 'put' method from list of methods to extract from filename

## [0.1.4]

- Does not crash if `file_sv.yaml` is empty
- Simplify virtual_server.rb code by splitting into methods
- Ability to make a dynamic path by prefixing folder with `%`. Makes a variable `@id` available to ERB in file

## [0.1.3] - 2021-03-05

- Ability to ignore_files from being processed with `global.ignore_files`

## [0.1.2] - 2021-03-04

- Fixed `version` method on exe
- Make plan show nicer view of what will be shown. `inspect` shows raw view
- Enable ability to use `file_sv.yaml` to configure defaults

## [0.1.1] - 2021-03-03

- Add Faker library for easy of random data
- Fixed issue with status code when no file_sv.yaml present

## [0.1.0] - 2021-02-26

- Initial release. Serving files based on file structure
