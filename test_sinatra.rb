# frozen_string_literal: true

require "sinatra"
require "puma"

#$logger = Logger.new

class MyApp < Sinatra::Base
  set :server, :puma
  #set :logging, false

  def serve
    [200, "Test #{@params['email']}"]
  end

  get "/" do
    puts 'get /'
    # logger.level = :error        
    # puts logger.class
    # puts logger.public_methods
    @params = params
    serve
  end
end

#MyApp.logging = nil
# puts MyApp.server.class
# puts MyApp.server.public_methods

MyApp.run!
